package hk.quantr.gitlab.indexer;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class GitlabLuceneIndexer {

	public static final Logger logger = Logger.getLogger(GitlabLuceneIndexer.class.getName());
	final File baseDir = new File("repo");
	Gitlab gitlab = new Gitlab();

	static {
		InputStream stream = GitlabLuceneIndexer.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption(Option.builder("a")
				.required(true)
				.hasArg()
				.argName("action")
				.desc("index/search")
				.longOpt("action")
				.build());
		options.addOption("c", "create", true, "create new index");
		options.addOption("u", "username", true, "gitlab username");
		options.addOption("p", "password", true, "gitlab password");
		options.addOption("t", "token", true, "gitlab token");
		options.addOption("s", "projects", true, "projects, separate by comma");
		options.addOption("q", "token", true, "search query");
		options.addOption("i", "index", true, "index folder path, for search only");
		options.addOption("o", "output", true, "output format, for search only");
		options.addOption("l", "url", true, "url startswith, for index only");
		options.addOption("f", "pattern", true, "index file name pattern");
		

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar gitlab-lucene-indexer-xx.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				logger.log(Level.INFO, ex.getMessage(), ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}

		new GitlabLuceneIndexer().start(options, args);
	}

	private void start(Options options, String[] args) {
		try {
			CommandLineParser cliParser = new DefaultParser();
			CommandLine cmd = cliParser.parse(options, args);

			if (cmd.getOptionValue("action").equals("index")) {
				if (cmd.hasOption("username")) {
					gitlab.username = cmd.getOptionValue("username");
				} else {
					System.out.println("Please input gitlab username?");
					gitlab.username = System.console().readLine();
				}
				if (cmd.hasOption("password")) {
					gitlab.username = cmd.getOptionValue("password");
				} else {
					System.out.println("Please input gitlab password?");
					gitlab.password = new String(System.console().readPassword());
				}
				if (cmd.hasOption("token")) {
					gitlab.token = cmd.getOptionValue("token");
				} else {
					if (new File(System.getProperty("user.home") + File.separator + "/.gitlab_token").exists()) {
						gitlab.token = IOUtils.toString(new FileInputStream(System.getProperty("user.home") + File.separator + "/.gitlab_token"), "utf-8").trim();
					} else {
						System.out.println("Please input gitlab token");
						gitlab.token = System.console().readLine();
					}
				}
				if (cmd.hasOption("projects")) {
					gitlab.pattern = cmd.getOptionValue("projects");
				}

				FileUtils.deleteDirectory(baseDir);
				if (cmd.getOptionValue("c") != null && cmd.getOptionValue("c").toLowerCase().equals("y")) {
					FileUtils.deleteDirectory(new File("index"));
				}
				SourceCodeIndexer indexer = new SourceCodeIndexer();

				String projects[] = Arrays.stream(gitlab.pattern.split(",")).map(String::trim).map(String::toLowerCase).toArray(String[]::new);

				JSONArray arr = gitlab.getProjects();
				for (int i = 0; i < arr.length(); i++) {
					JSONObject obj = (JSONObject) arr.get(i);
					String projectName = obj.getString("name");
					String gitUrl = obj.getString("http_url_to_repo");

					boolean b = false;
					for (String project : projects) {
						if (projectName.toLowerCase().contains(project) && (!cmd.hasOption("l") || gitUrl.toLowerCase().startsWith(cmd.getOptionValue("l")))) {
							b = true;
							break;
						}
					}
					if (b) {
//						System.out.println(CommonLib.prettyFormatJson(obj.toString()));
						logger.info(i + " : " + projectName + " = " + obj.getString("http_url_to_repo"));
						logger.info("git clone " + gitUrl);

						String repo_branches = obj.getJSONObject("_links").getString("repo_branches");
//						System.out.println("repo_branches=" + repo_branches);
						String json = gitlab.get(repo_branches, true);
//						System.out.println(json);
						JSONArray temp = new JSONArray(json);
						if (temp.length() > 0) {
							String branchName = temp.getJSONObject(0).getString("name");
//							System.out.println("branchName=" + branchName);
							String avatar_url = obj.isNull("avatar_url") ? "" : obj.getString("avatar_url");
							String web_url = obj.getString("web_url");

							File folder = new File(baseDir + File.separator + projectName);
							Git git = Git.cloneRepository()
									.setURI(gitUrl)
									.setDirectory(folder)
									.setCredentialsProvider(new UsernamePasswordCredentialsProvider(gitlab.username, gitlab.password))
									.call();
							git.close();
							indexer.index(projectName, branchName, avatar_url, web_url, gitUrl, folder, cmd.getOptionValue("f"));
							FileUtils.deleteDirectory(folder);
						} else {
							logger.log(Level.SEVERE, projectName + " has no branch");
						}
					}
				}
				FileUtils.deleteDirectory(baseDir);
			} else if (cmd.getOptionValue("action").equals("search")) {
				String q = cmd.getOptionValue("q");
				if (q == null) {
					System.out.println("please provide query by -q");
					return;
				}

				String myQuery = q;
				if (!q.startsWith("content:") && !q.contains(":")) {
					myQuery = "content:" + myQuery;
				}

				try {
					Analyzer analyzer = new StandardAnalyzer();

					QueryParser queryParser = new QueryParser("MyParser", analyzer);
//					Query query = queryParser.parse("content:peter@quantr.hk");
					Query query = queryParser.parse(myQuery);

					File indexFolder;
					if (cmd.hasOption("i")) {
						indexFolder = new File(cmd.getOptionValue("i"));
					} else {
						indexFolder = new File("index");
					}
					Directory directory = FSDirectory.open(indexFolder.toPath());
					IndexReader indexReader = DirectoryReader.open(directory);
					IndexSearcher searcher = new IndexSearcher(indexReader);
					TopDocs topDocs = searcher.search(query, 10);
					if (!cmd.hasOption("o") || !cmd.getOptionValue("o").equals("json")) {
						System.out.println("Search result: " + topDocs.totalHits);
						System.out.println("-".repeat(100));
					}
					ScoreDoc[] scoreDocs = topDocs.scoreDocs;
					JSONArray arr = new JSONArray();
					for (ScoreDoc scoreDoc : scoreDocs) {
						int docId = scoreDoc.doc;
						float score = scoreDoc.score;
						Document doc = searcher.doc(docId);

						if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
							JSONObject obj = new JSONObject();
							obj.put("category", doc.get("category"));
							obj.put("docID", docId);
							obj.put("score", score);
							obj.put("projectName", doc.get("projectName"));
							obj.put("web_url", doc.get("web_url"));
							obj.put("branchName", doc.get("branchName"));
							obj.put("avatar_url", doc.get("avatar_url"));
							obj.put("gitUrl", doc.get("gitUrl"));
							obj.put("filename", doc.get("filename"));
							obj.put("filepath", doc.get("filepath"));
							obj.put("content", doc.get("content"));

							JSONArray linesObj = new JSONArray();
							String content = doc.get("content");
							String lines[] = content.split("\r?\n");
							int lineNo = 0;
							for (String line : lines) {
								if (line.toLowerCase().contains(q.toLowerCase())) {
									JSONObject temp = new JSONObject();
									temp.put("lineNo", lineNo);
									temp.put("line", line);
									linesObj.put(temp);
								}
								lineNo++;
							}

							obj.put("lines", linesObj);
							arr.put(obj);
						} else {
							System.out.println("docID= " + docId + " , score= " + score);
							System.out.println("projectName: " + doc.get("projectName"));
							System.out.println("gitUrl: " + doc.get("gitUrl"));
							System.out.println("web_url: " + doc.get("web_url"));
							System.out.println("branchName: " + doc.get("branchName"));
							System.out.println("avatar_url: " + doc.get("avatar_url"));
							System.out.println("filename: " + doc.get("filename"));
							System.out.println("filepath: " + doc.get("filepath"));
							String content = doc.get("content");
							String lines[] = content.split("\r?\n");
							int lineNo = 0;
							for (String line : lines) {
								if (line.toLowerCase().contains(q.toLowerCase())) {
									System.out.println(lineNo + ": " + line);
								}
								lineNo++;
							}
							System.out.println("-".repeat(100));
						}
					}

					if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
						System.out.println(CommonLib.prettyFormatJson(arr.toString()));
					}
					indexReader.close();
				} catch (IOException | org.apache.lucene.queryparser.classic.ParseException ex) {
					ex.printStackTrace();
				}
			} else {
				System.out.println("wrong action");
			}
		} catch (FileNotFoundException ex) {
			logger.log(Level.SEVERE, null, ex);
		} catch (IOException | GitAPIException | ParseException ex) {
			logger.log(Level.SEVERE, null, ex);
		}
	}

}
