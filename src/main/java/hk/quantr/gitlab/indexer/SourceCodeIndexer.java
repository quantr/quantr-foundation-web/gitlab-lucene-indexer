package hk.quantr.gitlab.indexer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import me.tongfei.progressbar.ProgressBar;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SourceCodeIndexer {

	public void index(String projectName, String branchName, String avatar_url, String web_url, String gitUrl, File folder, String filenamePattern) {
//		System.out.println("filenamePattern=" + filenamePattern);
		try {
			List<Document> documents = new ArrayList<>();
			Iterator it;
			if (filenamePattern == null) {
				it = FileUtils.iterateFiles(folder, null, true);
			} else {
				String[] extensions = filenamePattern.split(",");
				it = FileUtils.iterateFiles(folder, extensions, true);
			}
			List<Object> list = new ArrayList<>();
			it.forEachRemaining(list::add);
//			for (Object obj : ProgressBar.wrap(list, "indexing " + folder.getName())) {

			try ( ProgressBar pb = new ProgressBar("indexing " + folder.getName(), list.size())) {
				for (Object obj : list) {
//			while (it.hasNext()) {
//				File file = (File) it.next();
//				logger.info("indexing : " + file.getName());
					File file = (File) obj;

					pb.step();
					pb.setExtraMessage("index " + file.getName());

					Document document = new Document();
					document.add(new StringField("category", "code", Field.Store.YES));
					document.add(new TextField("projectName", projectName, Field.Store.YES));
					document.add(new StringField("branchName", branchName, Field.Store.YES));
					document.add(new StringField("avatar_url", avatar_url, Field.Store.YES));
					document.add(new StringField("web_url", web_url, Field.Store.YES));
					document.add(new TextField("filename", file.getName(), Field.Store.YES));
					document.add(new StringField("filepath", file.getAbsolutePath().substring(folder.getAbsolutePath().length()), Field.Store.YES));
					document.add(new StringField("gitUrl", gitUrl, Field.Store.YES));
					document.add(new TextField("content", FileUtils.readFileToString(file, "utf-8"), Field.Store.YES));
					documents.add(document);

				}
				Analyzer analyzer = new StandardAnalyzer();
				IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
				File path = new File("index");
				Directory directory = FSDirectory.open(path.toPath());
				IndexWriter indexWriter = new IndexWriter(directory, indexConfig);
				for (Document doc : documents) {
					indexWriter.addDocument(doc);
				}
				indexWriter.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
