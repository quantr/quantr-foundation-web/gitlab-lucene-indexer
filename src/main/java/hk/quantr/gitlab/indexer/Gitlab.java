package hk.quantr.gitlab.indexer;

import static hk.quantr.gitlab.indexer.GitlabLuceneIndexer.logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Gitlab {

	public String username;
	public String password;
	public String token;
	public String pattern = "";

	final int gitlabApiTimeout = 5000;

	public Gitlab() {
	}

	public String get(String url, boolean injectToken) throws IOException {
		String str = null;
		do {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet;
			if (injectToken) {
				httpGet = new HttpGet(url + "?private_token=" + token);
			} else {
				httpGet = new HttpGet(url);
			}
			CloseableHttpResponse response = httpclient.execute(httpGet);
			try {
				str = IOUtils.toString(response.getEntity().getContent(), "utf8");
				if (str.contains("Too Many Requests")) {
					logger.log(Level.INFO, "Too Many Requests, sleep : " + url);
					try {
						Thread.sleep(gitlabApiTimeout);
					} catch (InterruptedException ex) {
						logger.log(Level.SEVERE, null, ex);
					}
				}
			} catch (Exception ex2) {
				logger.log(Level.INFO, "Exception, sleep : " + url);
				logger.log(Level.SEVERE, null, ex2);
				str = null;
				try {
					Thread.sleep(gitlabApiTimeout);
				} catch (InterruptedException ex) {
					logger.log(Level.SEVERE, null, ex);
				}
			}
			httpclient.close();
		} while (str == null || str.contains("Too Many Requests"));
		return str;
	}

	public JSONArray getProjects() throws FileNotFoundException, IOException {
		logger.info("get gitlab projects");
		int pageSize = 500;
		int pageNo = 1;
		JSONArray arr = new JSONArray();
		JSONArray temp;
		int noOfProject = 0;
		do {
			String json = get("https://gitlab.com/api/v4/projects?private_token=" + token + "&membership=true&order_by=name&per_page=" + pageSize + "&page=" + pageNo, false);
//		logger.log(Level.INFO, CommonLib.prettyFormatJson(json));
			temp = new JSONArray(json);
			noOfProject += temp.length();
			System.out.println("No of project: " + noOfProject);

			for (Object obj : temp) {
				arr.put(obj);
			}
			pageNo++;
		} while (temp.length() > 0);
		return arr;
	}
}
