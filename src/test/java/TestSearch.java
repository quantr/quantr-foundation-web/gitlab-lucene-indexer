
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSearch {

	@Test
	public void testSearch() {
//		List<Document> documents = new ArrayList<>();
//		String[] extensions = new String[]{"java", "ld"};
//		Iterator it = FileUtils.iterateFiles(new File("/Users/peter/workspace/Assembler"), extensions, true);
//		List<Object> list = new ArrayList<>();
//		it.forEachRemaining(list::add);
//		for (Object l: list){
//			System.out.println(l);
//		}

		try {
			Analyzer analyzer = new StandardAnalyzer();

			QueryParser queryParser = new QueryParser("MyParser", analyzer);
			Query query = queryParser.parse("content:peter@quantr.hk");

			Directory directory = FSDirectory.open(new File("index").toPath());

			IndexReader indexReader = DirectoryReader.open(directory);

			IndexSearcher searcher = new IndexSearcher(indexReader);

			TopDocs topDocs = searcher.search(query, 10);

			System.out.println("Search result: " + topDocs.totalHits);
			System.out.println("-".repeat(100));
			ScoreDoc[] scoreDocs = topDocs.scoreDocs;
			for (ScoreDoc scoreDoc : scoreDocs) {
				int docId = scoreDoc.doc;
				float score = scoreDoc.score;
				System.out.println("docID= " + docId + " , score= " + score);
				Document doc = searcher.doc(docId);
				System.out.println("filename: " + doc.get("filename"));
				System.out.println("filepath: " + doc.get("filepath"));
				System.out.println("-".repeat(100));
			}

			// 8. 关闭资源
			indexReader.close();
		} catch (IOException | ParseException ex) {
			ex.printStackTrace();
		}
	}
}
