
import hk.quantr.gitlab.indexer.Gitlab;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGitLabAPI {

	@Test
	public void test() throws IOException {
		String token = IOUtils.toString(new FileInputStream(System.getProperty("user.home") + File.separator + "/.gitlab_token"), "utf-8").trim();
		String url = "https://gitlab.com/api/v4/projects/8071639/repository/branches?private_token=" + token;
		System.out.println(new Gitlab().get(url, false));
	}
}
