# GitLab Lucene Indexer

Index gitlabe project and search it

## Index

java -jar target/gitlab-lucene-indexer-1.0.jar -c y -s assembler,simulator,elf,verilog,kernel,dwarf,peter-ar,quantr-java,vcd -l https://gitlab.com/quantr/toolchain -f java,c,cpp,ld,v,js,ts,jsx,tsx -a index

java -jar target/gitlab-lucene-indexer-1.0.jar -l https://gitlab.com/quantr/sharepoint -a index

### Build index for www.quantr.foundation

```
cd /home/quantr.foundation/www
java -jar ~/workspace/gitlab-lucene-indexer/target/gitlab-lucene-indexer-1.0.jar -c y -s assembler,simulator,elf,verilog,kernel,dwarf,peter-ar,java,vcd,quantr-logic,quantr-i,doclet,qemu -l https://gitlab.com/quantr/toolchain -f java,c,cpp,ld,v,js,ts,jsx,tsx -a index
rm -fr lucene_index
mv index lucene_index
```

## Search

java -jar target/gitlab-lucene-indexer-1.0.jar -a search -q peter

java -jar target/gitlab-lucene-indexer-1.0.jar -a search -q peter -o json

java -jar target/gitlab-lucene-indexer-1.0.jar -a search -i <index folder> -q peter

